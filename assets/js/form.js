const formContainer = document.getElementById('form-container');
const form = formContainer.dataset.prototype;
const buttonAddFile = document.getElementById('addFile');
let index = 0;
const regex = /__name__/g;

buttonAddFile.addEventListener('click', (e)=>{
   e.preventDefault();
   index++;
   li = document.createElement('li');
   let newForm = form.replace(regex, 'uploadFile' + index);
   li.innerHTML = newForm;
   formContainer.appendChild(li);

});
