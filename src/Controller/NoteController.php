<?php

/*
 * This file is part of the project symfony-sandbox.
 */

namespace App\Controller;

use App\Entity\Note;
use App\Form\NoteType;
use App\Repository\NoteRepository;
use App\Repository\UploadFileRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class NoteController extends AbstractController
{
    /**
     * @Route("/", name="note")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request, NoteRepository $noteRepository, UploadFileRepository $fileRepository, EntityManagerInterface $em, PaginatorInterface $paginator)
    {
        $notes = $noteRepository->findAll();
        $pagination = $paginator->paginate(
            $notes,
            $request->query->getInt('page', 1),
            3
        );

        return $this->render('note/index.html.twig', [
            'pagination' => $pagination,
        ]);
    }

    /**
     * @Route("/create", name="note_create")

     *
     * @param NoteRepository $entityManager
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(Request $request, EntityManagerInterface $entityManager)
    {
        $note = new Note();
        $form = $this->createForm(NoteType::class, $note);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            foreach ($note->getUploadFiles() as $uploadFile) {
                $uploadFile->setNote($note);
                $entityManager->persist($uploadFile);
            }
            $entityManager->persist($note);

            $entityManager->flush();

            return $this->redirectToRoute('note');
        }

        return $this->render('note/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/show/{note}", name="note_show")
     */
    public function show(Note $note): Response
    {
        return $this->render('note/show.html.twig', [
            'note' => $note,
        ]);
    }

    /**
     * @Route("/delete/{note}", name="note_delete")
     */
    public function delete(Note $note, Request $request, EntityManagerInterface $entityManager): Response
    {
        $entityManager->remove($note);
        $entityManager->flush();

        return $this->redirectToRoute('note');
    }
}
